// const nock = require('nock')

// nock('https://localhost:8080')
//   .get(/.*/)
//   .reply(200, {
//     ok: true
//   })
//   .persist()

module.exports = (http, vars) => ({
  target: 'http://localhost:3000',
  environments: [{
    test: {
      target: 'https://localhost:8081'
    }
  }],
  duration: 10,
  parallel: {
    count: 5000,
    concurrency: 32,
    steps: [
      (options) => http.get(options)
    ]
  }
})
