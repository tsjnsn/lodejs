const nock = require('nock')

nock('https://localhost:8080')
  .get(/.*/)
  .delay(150)
  .reply(200, {
    ok: true
  })
  .persist()

module.exports = async (http, vars) => ({
  target: 'https://localhost:8080',
  environments: [{
    test: {
      target: 'https://localhost:8081'
    }
  }],
  count: 100,
  name: 'gets',
  sequential: [
    (target) => http.get(`${target}`),
    (target) => http.get(`${target}`)
  ]
})
