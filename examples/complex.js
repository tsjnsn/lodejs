const nock = require('nock')

nock('https://localhost:8080')
  .get(/.*/)
  .delay(150)
  .reply(200, {
    ok: true
  })
  .persist()

module.exports = async (http, vars) => ({
  target: 'https://localhost:8080',
  environments: [{
    test: {
      target: 'https://localhost:8081'
    }
  }],
  count: 100,
  parallel: [{
    name: 'gets',
    sequential: [
      (options) => http.get(options),
      (options) => http.get(options)
    ]
  }, {
    name: 'more gets',
    sequential: [{
      count: 2,
      sequential: [
        (options) => http.get(options),
        (options) => http.get(options)
      ]
    }, {
      count: 1,
      sequential: [
        (options) => http.get(options)
      ]
    }]
  }]
})
