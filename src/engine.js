const minimist = require('minimist')
const request = require('request-promise')
const debug = require('debug')('lodejs')
const _ = require('underscore')
const events = require('events')
const dl = require('datalib')
const { performance } = require('perf_hooks')
const cluster = require('cluster')
const numCPUs = require('os').cpus().length
const Promise = require('bluebird')
const httpx = require('http')
const httpsx = require('https')

let processIndex = 0

const args = minimist(process.argv.slice(2))
debug({ args })

const http = {
  get: async (options) => {
    const start = performance.now()
    const result = await request.get({
      url: options.target,
      resolveWithFullResponse: true,
      time: true,
      agent: options.agent,
      timeout: options.notAfter - start
    }).catch(err => err)
    if (result.error) return result
    delete result.body
    delete result.toString
    return { engine: 'http', method: 'get', target: options.target, statusCode: result.statusCode, responseTime: result.timings.response, timings: { rlt: performance.now() - start, duration: performance.now() - options.startTime, ...result.timings }, timingPhases: result.timingPhases }
  },
  noop: async () => {
    const start = performance.now()
    await new Promise((resolve, reject) => resolve())
    return { engine: 'noop', responseTime: performance.now() - start }
  },
  post: async (target, body) => request.post({
    url: target,
    resolveWithFullResponse: true,
    time: true
  })
}

function combinations (arr1, arr2) {
  let arr = []
  for (const item1 of arr1) {
    for (const item2 of arr2) {
      arr.push([item1, item2])
    }
  }
  return arr
}

const stepTypes = {
  parallel: {
    execute: async ({ name, count = 1, steps, concurrency }, options, ee, context = '') => {
      concurrency = concurrency || options.agent.maxSockets
      const results = await Promise.map(combinations(_.range(1, count + 1), steps), ([repetition, step], i) => {
        if (performance.now() < options.notAfter) {
          return executeSteps(step, options, ee, `${context}${name ? `["${name}"]` : ''}/parallel#${repetition}[${i}]`)
        }
        return { error: true }
      }, { concurrency })
      // console.log('Errors: %o', results.filter(x => !x.error))
      return { name, parallel: results.filter(x => !x.error) }
    }
  },

  sequential: {
    execute: async ({ name, count = 1, steps }, options, ee, context = '') => {
      let results = []
      for (let repetition = 1; repetition < count + 1; repetition++) {
        for (let i = 0; i < steps.length; i++) {
          const newContext = `${context}${name ? `["${name}"]` : ''}/sequential#${repetition}[${i}]`
          results.push(await executeSteps(steps[i], options, ee, newContext))
        }
      }
      return { name, sequential: results }
    }
  }
}

async function executeSteps (step, options, ee, context = '') {
  if (typeof step === 'function') {
    debug(`${context}/step`)

    const result = await step(options)
    if (!result.error) {
      process.send({ cmd: 'requestComplete', result, round: options.seconds })
      ee.emit('step', result)
    } else {
      process.send({ cmd: 'requestFailed', error: result.error, round: options.seconds })
    }
    return result
  }

  let { parallel, sequential } = step
  if (parallel) {
    return stepTypes.parallel.execute(parallel, options, ee, context)
  }

  if (sequential) {
    return stepTypes.sequential.execute(sequential, options, ee, context)
  }
}

function recordResult (stats, result) {
  stats.push({
    method: result.method,
    statusCode: result.statusCode,
    responseTime: result.responseTime,
    ...result.timings,
    ...result.timingPhases
  })
}

function recordFailure (stats, error) {
  stats.push({
    ...error
  })
}

function printSummary (stats) {
  console.log('')
  console.log('')
  console.log(`Summary`)
  console.log(`-------`)
  console.log('')
  console.log(`Completed ${stats.results.length} requests in ${(stats.duration / 1000).toFixed(2)} seconds = ${stats.results.length / (stats.duration / 1000)}/s`)
  console.log('')
  // const overviewData = dl.read(stats.overviewStats, { type: 'json' })
  // console.log(dl.print.summary(overviewData))
  const timingData = dl.read(stats.results.map(stat => ({ response: stat.response })), { type: 'json' })
  console.log(dl.print.summary(timingData))
}

function printRoundSummary ({ round, duration, results, errors }) {
  console.log('')
  console.log(`Round ${round}:`)
  console.log(`Completed ${results.length} requests in ${(duration / 1000).toFixed(2)} seconds = ${results.length / (duration / 1000)}/s`)
  console.log(`Failed ${errors.length} requests`)
  const errorData = dl.read(errors.map(x => ({ code: x.code })), { type: 'json' })
  console.log(dl.print.summary(errorData))
  // const overviewData = dl.read(stats.overviewStats, { type: 'json' })
  // console.log(dl.print.summary(overviewData))
  // const timingData = dl.read(stats.timingStats, { type: 'json' })
  // console.log(dl.print.summary(timingData))
}

async function execute (configFunc, ee, seconds = 1, timing = 0) {
  const config = require(configFunc)(http, ee)
  const now = performance.now()
  if (timing && now - timing > 1100) {
    console.log(`Ms since last iteration: ${now - timing}`)
  }

  if (config.duration > seconds) setTimeout(execute, 1000, configFunc, ee, seconds + 1, now)

  const options = {
    agent: new httpx.Agent({
      keepAlive: true,
      maxSockets: 100
    }),
    seconds,
    target: config.target,
    startTime: now,
    notAfter: now + 1000
  }

  config.count = Math.floor(config.count / numCPUs)

  if (processIndex < config.count % numCPUs) {
    config.count++
  }

  await executeSteps(config, options, ee, `<${process.pid}> `.padEnd(8))
  process.send({ cmd: 'roundDone', round: seconds, duration: performance.now() - now })

  if (config.duration <= seconds) {
    process.send({ cmd: 'done' })
  }
}

async function main () {
  const ee = new events.EventEmitter()
  // ee.on('step', function (result) {
  //   recordResult(result)
  // })

  process.on('message', async function (msg) {
    if (msg.cmd === 'start') {
      processIndex = msg.index
      await execute(args.config, ee)
    }
  })
}

if (cluster.isMaster) {
  let start = null
  console.log(`Master ${process.pid} is running`)

  // Fork workers.
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork()
  }

  let waiting = numCPUs
  const stats = {
    resultsByRound: {},
    results: [],
    failures: []
  }
  const messageHandler = (msg) => {
    if (msg.round) {
      if (!stats.resultsByRound[msg.round]) {
        stats.resultsByRound[msg.round] = {
          results: [],
          errors: [],
          workerResults: []
        }
      }
    }

    if (msg.cmd && msg.cmd === 'requestComplete') {
      recordResult(stats.resultsByRound[msg.round].results, msg.result)
      recordResult(stats.results, msg.result)
    }
    if (msg.cmd && msg.cmd === 'requestFailed') {
      recordFailure(stats.resultsByRound[msg.round].errors, msg.error)
      recordFailure(stats.failures, msg.error)
    }
    if (msg.cmd && msg.cmd === 'roundDone') {
      stats.resultsByRound[msg.round].workerResults.push(msg)
      if (stats.resultsByRound[msg.round].workerResults.length >= numCPUs) {
        stats.resultsByRound[msg.round].duration = Math.max(...stats.resultsByRound[msg.round].workerResults.map(r => r.duration))
        stats.resultsByRound[msg.round].round = msg.round
        printRoundSummary(stats.resultsByRound[msg.round])
      }
    }
    if (msg.cmd && msg.cmd === 'done') {
      if (--waiting === 0) {
        stats.duration = performance.now() - start
        printSummary(stats)
        process.exit(0)
      }
    }
  }

  start = performance.now()
  for (const id in cluster.workers) {
    cluster.workers[id].on('message', messageHandler)
    cluster.workers[id].send({ cmd: 'start', index: id - 1 })
    // workerResults[worker] = []
  }

  // cluster.on('exit', (worker, code, signal) => {
  //   console.log(`worker ${worker.process.pid} died`)
  // })
} else {
  // Workers can share any TCP connection
  // In this case it is an HTTP server
  main()

  console.log(`Worker ${process.pid} started`)
}

// main()
